﻿using Glowny.Kontrakt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wyswietlacz.Kontrakt;

namespace Glowny.Implementacja
{
    public class Glowny : IGlowny
    {
        private IWyswietlacz wyswietlacz;
        public IWyswietlacz Wyswietlacz { get; set; }

        public Glowny(IWyswietlacz wyswietlacz)
        {
            this.wyswietlacz = wyswietlacz;
        }

        public void StworzOkno(string tekst)
        {
            wyswietlacz.Stworz(tekst);
        }

        public void GlownyM2()
        {
            throw new NotImplementedException();
        }

    }
}
