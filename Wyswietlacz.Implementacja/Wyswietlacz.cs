﻿using Lab5.DisplayForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Wyswietlacz.Kontrakt;

namespace Wyswietlacz.Implementacja
{
    public class Wyswietlacz : IWyswietlacz
    {
        private DisplayViewModel display;
        public DisplayViewModel Display { get { return display; } set { display = value; } }

        public Wyswietlacz(DisplayViewModel display)
        {
            this.display = display;
        }

        public void Stworz(string tekst)
        {
            display = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
                {
                    var form = new Form();
                    var viewModel = new DisplayViewModel();
                    form.DataContext = viewModel;
                    form.Show();
                    return viewModel;
                }), null);
            display.Text = tekst;
        }

        public void WyswietlaczM2()
        {
            throw new NotImplementedException();
        }
    }
}
