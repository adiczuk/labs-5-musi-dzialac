﻿using System;
using PK.Container;
using Wyswietlacz.Kontrakt;
using Glowny.Kontrakt;
using Lab5.DisplayForm;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer kontener = new Container();

            IWyswietlacz wyswietlacz = new Wyswietlacz.Implementacja.Wyswietlacz(new DisplayViewModel());
            IGlowny glowny = new Glowny.Implementacja.Glowny(wyswietlacz);

            kontener.Register(wyswietlacz);
            kontener.Register(glowny);

            return kontener;
        }
    }
}
