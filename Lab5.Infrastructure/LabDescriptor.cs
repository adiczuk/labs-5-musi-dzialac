﻿using Glowny.Kontrakt;
using Wyswietlacz.Kontrakt;
using PK.Container;
using System;
using System.Reflection;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Glowny.Implementacja.Glowny));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietlacz.Implementacja.Wyswietlacz));

        #endregion
    }
}
